<?php
//Produced by haku -hphな休日- http://www.p-ho.net/ [p-ho AnnualCalendar]
mb_language("ja");
mb_internal_encoding("UTF-8");
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>AnnualCalendar</title>
</head>
<body>
<?php
if ($_POST["nen"]) { 																					//"年" がPOSTされてきたら
$nen = $_POST["nen"]; 																					//POSTの値を受け取って変数に割り当てる
}
else {
$nen = date("Y");																						//今日の"年"
}
$youbi = array("日","月","火","水","木","金","土");														//曜日を配列に入れる
$tdiv = 0;																								//テーブルの数を数えるための変数
for ($gatsu = 1; $gatsu < 13; $gatsu++) {																//月に１を足しながら12月まで繰り返し
$hi = 1;																								//"日"は 1 から始める
$you = date("w", mktime(0, 0, 0, $gatsu, $hi, $nen));													//１日の曜日
if ($tdiv == 0) {																						//一番左側のテーブル
print "<table border='0'>\n<tr>\n<td>\n<div style='float:left;margin-right:20px;margin-bottom:8px;'>";
$tdiv++;																								//$tdiv の値に＋１します
}
elseif (($tdiv == 1) or ($tdiv == 2)) {																	//２番目３番目のテーブル
print "<div style='float:left;margin-right:20px;margin-bottom:8px;'>";
$tdiv++;																								//$tdiv の値に＋１します
}
else {																									//テーブルが４つ目になったら
print "<div style='margin-bottom:8px;'>";
$tdiv = 0;																								//次の段へ
}
?>
<table border="1">
<tr>
<td colspan="7" align="center">
<?php
print $nen . "年" . $gatsu . "月</td>\n";
?>
</tr>
<tr>
<td align="center" bgcolor="#ffc0cb"><span style="font-size=85%">日</span></td>
<td align="center"><span style="font-size=85%">月</span></td>
<td align="center"><span style="font-size=85%">火</span></td>
<td align="center"><span style="font-size=85%">水</span></td>
<td align="center"><span style="font-size=85%">木</span></td>
<td align="center"><span style="font-size=85%">金</span></td>
<td align="center" bgcolor="#b0c4de"><span style="font-size=85%">土</span></td>
</tr>
<tr>
<?php
if ($hi == 1) {																							//１日だったら
$yo = 0;																								//初期値を 0 として変数 $yo を設定
for ($i = $yo; $i < $you; $i++) {																		//正しい曜日まで繰り返す
print "<td>&nbsp;</td>";																				//空白セルを出力
}
}
$monday = 0;																							//初期値を 0 として変数 $yo を設定
for ($i = $hi; $i < 40; $i++) {																			//日付に１を足しながら40日まで繰り返し
$you = date("w", mktime(0, 0, 0, $gatsu, $hi, $nen));													//曜日
if ($you == 1) {																						//月曜日なら$mondayに＋１
$monday++;
}
$y2 = ($nen - 2000);
$syunbun = (int)(20.69115 + 0.2421904 * $y2 - (int)($y2/4 + $y2/100 + $y2/400));
$syuubun = (int)(23.09000 + 0.2421904 * $y2 - (int)($y2/4 + $y2/100 + $y2/400));
if (($nen == date("Y")) && ($gatsu == date("n")) && ($hi == date("d"))) { 					//今日なら
print "<td align='center' bgcolor='#8fbc8f'>" . $hi . "</td>\n"; 						//背景を darkseagreen に
}
elseif (($gatsu == 1) && ($hi == 1)) {										//元日（1月1日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='元日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 1) && ($hi == 2) && ($you == 1)) {								//元日の振替休日（1月2日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 1) && ($monday == 2) && ($you == 1)) {							//成人の日（1月の第2月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='成人の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 2) && ($hi == 11)) {										//建国記念の日（2月11日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='建国記念の日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif (($gatsu == 2) && ($hi == 12) && ($you == 1)) {								//建国記念の日の振替休日（2月12日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($nen  > 1999 ) && ($gatsu == 3) && ($hi == $syunbun)) {						//春分の日（計算式による）
print "<td align='center' bgcolor='#ffc0cb'><span title='春分の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($nen  > 1999 ) && ($gatsu == 3) && ($hi == ($syunbun + 1)) && ($you == 1)) {				//春分の日の振替休日
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($nen < 2007) && ($gatsu == 4) && ($hi == 29)) {							//2006年みどりの日（4月29日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='みどりの日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif (($nen < 2007) && ($gatsu == 4) && ($hi == 30) && ($you == 1)) {						//みどりの日の振替休日（4月30日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($nen > 2006) && ($gatsu == 4) && ($hi == 29)) {							//昭和の日（4月29日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='昭和の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($nen > 2006) && ($gatsu == 4) && ($hi == 30) && ($you == 1)) {						//昭和の日の振替休日（4月30日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 5) && ($hi == 3)) {										//憲法記念日（5月3日なら）
if (($nen > 2006) && ($you == 0)) {										//憲法記念日が日曜なら
$kokuminf = on;													//振替休日有りの印を付ける
}
print "<td align='center' bgcolor='#ffc0cb'><span title='憲法記念日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif ((($nen < 2007) && ($gatsu == 5) && ($hi == 4) && ($you == 2)) or (($nen < 2007) && ($gatsu == 5) && ($hi == 4) && ($you == 3)) or (($nen < 2007) && ($gatsu == 5) && ($hi == 4) && ($you == 4)) or (($nen < 2007) && ($gatsu == 5) && ($hi == 4) && ($you == 5)) or (($nen < 2007) && ($gatsu == 5) && ($hi == 4) && ($you == 6))) { //国民の休日（5月4日が火～土曜日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='国民の休日'>" . $hi . "</span></td>\n"; 		//背景を pink に
}
elseif (($nen > 2006) && ($gatsu == 5) && ($hi == 4)) {								//2007年以降みどりの日（5月4日なら）
if (($nen > 2006) && ($kokuminf != on) && ($you == 0)) {							//みどりの日が日曜なら
$kokuminf = on;													//振替休日有りの印を付ける
}
print "<td align='center' bgcolor='#ffc0cb'><span title='みどりの日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif (($gatsu == 5) && ($hi == 5)) {										//こどもの日（5月5日なら）
if (($nen > 2006) && ($kokuminf != on) && ($you == 0)) {							//こどもの日が日曜なら
$kokuminf = on;													//振替休日有りの印を付ける
}
print "<td align='center' bgcolor='#ffc0cb'><span title='こどもの日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif (($nen < 2007) && ($gatsu == 5) && ($hi == 6) && ($you == 1)) {						//こどもの日の振替休日（5月6日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'>" . $hi . "</span></td>\n";						//背景を pink に
}
elseif (($nen > 2006) && ($kokuminf == on) && ($gatsu == 5) && ($hi == 6)) {					//３連祝日のどれかが日曜なら振替休日
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 7) && ($monday == 3) && ($you == 1)) {							//海の日（7月の第3月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='海の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($nen > 2015) && ($gatsu == 8) && ($hi == 11)) { 							//2016年以降、山の日（8月11日）なら
print "<td align='center' bgcolor='#ffc0cb'><span title='山の日'>" . $hi . "</span></td>\n"; 			//背景を pink に
}
elseif (($nen > 2015) && ($gatsu == 8) && ($hi == 12) && ($you == 1)) { 					//山の日の振替休日（8月12日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n"; 			//背景を pink に
} 
elseif (($gatsu == 9) && ($monday == 3) && ($you == 1)) {							//敬老の日（9月の第3月曜なら）
$keiro = $hi;													//敬老の日の日付（国民の祝日の有無確認のため）
if (($syuubun - $keiro) == 2) {											//敬老の日の２日後が秋分の日なら
$kokumin = $syuubun - 1;
}
print "<td align='center' bgcolor='#ffc0cb'><span title='敬老の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($kokumin) && (($gatsu == 9) && ($hi == $kokumin))) {							//９月の国民の休日が有りなら
print "<td align='center' bgcolor='#ffc0cb'><span title='国民の休日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif (($nen  > 1999 ) && ($gatsu == 9) && ($hi == $syuubun)) {						//秋分の日（計算式による）
print "<td align='center' bgcolor='#ffc0cb'><span title='秋分の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($nen  > 1999 ) && ($gatsu == 9) && ($hi == ($syuubun + 1)) && ($you == 1)) {				//秋分の日の振替休日
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 10) && ($monday == 2) && ($you == 1)) {							//体育の日（10月の第2月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='体育の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 11) && ($hi == 3)) {										//文化の日（11月3日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='文化の日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 11) && ($hi == 4) && ($you == 1)) {								//文化の日の振替休日（11月4日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 11) && ($hi == 23)) {									//勤労感謝の日（11月23日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='勤労感謝の日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif (($gatsu == 11) && ($hi == 24) && ($you == 1)) {								//勤労感謝の日の振替休日（11月24日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif (($gatsu == 12) && ($hi == 23)) {									//天皇誕生日（12月23日なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='天皇誕生日'>" . $hi . "</span></td>\n";		//背景を pink に
}
elseif (($gatsu == 12) && ($hi == 24) && ($you == 1)) {								//天皇誕生日の振替休日（12月24日が月曜なら）
print "<td align='center' bgcolor='#ffc0cb'><span title='振替休日'>" . $hi . "</span></td>\n";			//背景を pink に
}
elseif ($you == 0) {												//日曜日なら
print "<td align='center' bgcolor='#ffc0cb'>" . $hi . "</td>\n";						//背景を pink に
}
elseif ($you == 6) {												//土曜日なら
print "<td align='center' bgcolor='#b0c4de'>" . $hi . "</td>\n";						//背景を lightsteelblue に
}
else {
print "<td align='center'>" . $hi . "</td>\n";									//平日表示
}
$hi++;
if (checkdate($gatsu, $hi, $nen) == false) { 									//＋１した日付がありえない日付だったら
break; 														//繰り返し処理を抜ける
}
if ($you == 6) {												//土曜日なら
print "</tr>\n<tr>\n";												//行変えする
}
}
$hi = $hi -1;													//行き過ぎた日を１日戻す
$you = date("w", mktime(0, 0, 0, $gatsu, $hi, $nen));								//曜日を配列に
if ($you < 6) {
$yo = $you;
for ($yo; $yo < 6; $yo++) {											//カレンダー１列分（6）まで繰り返す
print "<td>&nbsp;</td>";											//空白セルを出力
}
}
?>
</tr>
</table>
</div>
<?php
if ($tdiv == 0) {												//段の終わり（４つめの月）では
print "</td>\n</tr>\n</table>\n";										//テーブルを閉める
}
}
?>
<form method="POST" action="AnnualCalendar.php">
<select name="nen">
<option <?php if ($nen == "2003") { print "selected";} ?> value="2003">2003</option>
<option <?php if ($nen == "2004") { print "selected";} ?> value="2004">2004</option>
<option <?php if ($nen == "2005") { print "selected";} ?> value="2005">2005</option>
<option <?php if ($nen == "2006") { print "selected";} ?> value="2006">2006</option>
<option <?php if ($nen == "2007") { print "selected";} ?> value="2007">2007</option>
<option <?php if ($nen == "2008") { print "selected";} ?> value="2008">2008</option>
<option <?php if ($nen == "2009") { print "selected";} ?> value="2009">2009</option>
<option <?php if ($nen == "2010") { print "selected";} ?> value="2010">2010</option>
<option <?php if ($nen == "2011") { print "selected";} ?> value="2011">2011</option>
<option <?php if ($nen == "2012") { print "selected";} ?> value="2012">2012</option>
<option <?php if ($nen == "2013") { print "selected";} ?> value="2013">2013</option>
<option <?php if ($nen == "2014") { print "selected";} ?> value="2014">2014</option>
<option <?php if ($nen == "2015") { print "selected";} ?> value="2015">2015</option>
<option <?php if ($nen == "2016") { print "selected";} ?> value="2016">2016</option>
<option <?php if ($nen == "2017") { print "selected";} ?> value="2017">2017</option>
<option <?php if ($nen == "2018") { print "selected";} ?> value="2018">2018</option>
<option <?php if ($nen == "2019") { print "selected";} ?> value="2019">2019</option>
<option <?php if ($nen == "2020") { print "selected";} ?> value="2020">2020</option>
<option <?php if ($nen == "2021") { print "selected";} ?> value="2021">2021</option>
<option <?php if ($nen == "2022") { print "selected";} ?> value="2022">2022</option>
<option <?php if ($nen == "2023") { print "selected";} ?> value="2023">2023</option>
<option <?php if ($nen == "2024") { print "selected";} ?> value="2024">2024</option>
<option <?php if ($nen == "2025") { print "selected";} ?> value="2025">2025</option>
<option <?php if ($nen == "2026") { print "selected";} ?> value="2026">2026</option>
<option <?php if ($nen == "2027") { print "selected";} ?> value="2027">2027</option>
</select> 年　
<input type="submit" value="Go">
</form>
</body>
</html>
